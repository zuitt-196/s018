
// function able to receive data without the use of global variblaes or promtt();

// name --> is a parameter
//parameter -->is varible/ container that exixts only in our function is uesd to stor information that  provide to a function when it is called/invoked
function printName(name) {
        console.log("My name is " + name)
}

// data passed intop a function invocation can be recieved by the funtionm
// this is what we call argument 
printName("vhong"); // --> evoke the printName() where add the value function
printName("jimin");// --> argument called

// data passed into the function through function invocation is called
//The argumnet is then stored  within a container called a parameter


function prinMyAge(age) {//-> it is scope function 
    console.log('I am ' + age)
}

prinMyAge(23);
prinMyAge();



// check divisibility reusably using a functiom with arguments and parameter 
function checckDivicibltyByb(num) {
        let remainder = num % 8;
        console.log("the remainder of " + num + " divided by 8 is: " + remainder);
        let isDivisibilty8 = remainder === 0;
        console .log("is " + num + " Divisible by 8");
        console .log(isDivisibilty8);
}

checckDivicibltyByb(64);
checckDivicibltyByb(27);


//mini activyt 

/*
    1.Crete a function which is able to recieve data as an argument 
    -this function shoukld a=be able to recieve the name of yopur favorite superhero



    2.Dbug the follwing function and invoke the method with an appropraiote argumnent

    
*/

//1 
function superHero(hero) {
    console.log("My favorite superHero is " + hero)
}
superHero('spiderman');
superHero('Batman');
superHero('Thor');



//2.
function printMyfavoriteLanguage(language) {
    console.log("my favorite language is:" + language);

}
printMyfavoriteLanguage("js")
printMyfavoriteLanguage("php")
printMyfavoriteLanguage("pyton")



// Multiple argument can also be passed intp a function; Multple parameter containe our arguyment
 

function printFullName(firstName,middleName,LastName){
    console.log(firstName + " " + middleName + " " + LastName);
}
printFullName('juan','cristomo','Ibarra');
printFullName('Ibarra','juan','Cristomo');

/*

    parameter will contain the argument according to the order it was passed

    "juan" - firstName
    "cristomo" - middleName
    "ibarra" - lastaname
    
    In other language , proiding more/less arguiments than the expectted parameter sometimes capouse an error change thge bahaior od of the function.

    in Javasxrpit, we dont have to worry about that.

    im Javascrpit, providing less arguments than the expected parameter will automativalyy assign an undefined value to the parameter
    */

    printFullName("Stephen","Wardell")
    printFullName("Stephen","Wardell","Curry", 'james');



    // use th varible as argumnent              
    let fName = "larry";
    let mName = 'joe';
    let lName = 'bird';

    printFullName(fName,mName,lName);


    /*  
        Mini-Acivity

        create  a function which will be able to recieve 5 argument
        -recieve 5 of your favorite songs

    */

     function printfavorateSong(song1,song2,song3,song4,song5) {
        console.log("My favorate Song is:")
        console.log(song1);
        console.log(song2);
        console.log(song3);
        console.log(song4);
        console.log(song5);


     }

     printfavorateSong("1. Sana",'2. ikaw lang ', '3. i ll be', '4.forevermore', 'just one');

    //  Return 

    //Currenctyl or so far, our function are able toi dislpay data  i our console.
    // howevrm pur funtion cannot yet return value, function are able to return value which
    
     

    let fullName = printFullName("vhong",'bercasio', 'aguilon');
    console.log(fullName);


    function returnFullName(firstName, middleName,lastName) {
            return firstName + " " + middleName + " " + lastName; // ----> return the store value uesd by  the keyword statement  return 


    }
     

    // let booleanSample = 1===0 ; it is sample of return value
    fullName = returnFullName('Enersto','Antonio' ,'Maceda');
    console.log(fullName);
    console.log(fullName + "is my grandName");


    function returnPhiliphinesAddress(city) {

        return city + ",Philipphines"
        
    }


    // return -return a value from a function which we can save in a variable 
    let myFullAdress = returnPhiliphinesAddress("cainta");
        console.log(myFullAdress);




    // returns true if number is diviible by 4, else, return false
        function divisibiltyBY4(num) {
                let remainder = num % 4;
                 let isDivisibilty4 = remainder === 0
        
                // return either true or false 
                // not only can you return raw valur/fata, you can also direclty return a variable
                 return isDivisibilty4;
                //  return keyword not only allwos us to return value but also ends the process of our funtion 

               
        }


        let num4isDivisibiltyBy4 = divisibiltyBY4(4);
        let num14isDivisibiltyBy4 = divisibiltyBY4(14);

        console.log(num4isDivisibiltyBy4)
        console.log(num14isDivisibiltyBy4);


        function createPlayerInfo(username,level,job) {
              return "username: " + username + ", level: " + level + ", job: " + job;
                // console.log("username: " + username + ", level: " + level + ", job: " + job);
        }


        let user1 = createPlayerInfo("white_night",95,"Paladin");
        console.log(user1);